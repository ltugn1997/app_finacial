import 'package:flutter/material.dart';
import 'package:financeappui/models/history_chart.dart';

class PaymentMenthodChart{
  final double totalSpendMoney;
  final List<ChartItem> chartPaymentMenthod;

  PaymentMenthodChart({
    @required this.totalSpendMoney,
    @required this.chartPaymentMenthod,
  });
}