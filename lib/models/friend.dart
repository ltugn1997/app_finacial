import 'package:flutter/material.dart';

class Friend {
  final String id;
  final String name;
  final String image;

  Friend({
    @required this.id,
    @required this.name,
    @required this.image,
  });
}