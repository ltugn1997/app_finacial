import 'package:flutter/material.dart';

class PaymentMethod {
  final String id;
  final String methodName;
  final double totalBalance;
  final String logo;
  final String infoMethod;

  PaymentMethod({
    @required this.id,
    @required this.methodName,
    @required this.totalBalance,
    @required this.logo,
    @required this.infoMethod,
  });

  PaymentMethod copyWith({
    final String id,
    final String methodName,
    final double totalBalance,
    final String logo,
    final String infoMethod,
  }){
    return PaymentMethod(
      id: id ?? this.id,
      methodName: methodName ?? this.methodName,
      totalBalance: totalBalance ?? this.totalBalance,
      logo: logo ?? this.logo,
      infoMethod: infoMethod ?? this.infoMethod,
    );
  }
}