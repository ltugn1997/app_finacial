import 'package:flutter/material.dart';

class HistoryTransaction {
  final String id;
  final String nameAction;
  final double cost;
  final String image;
  final String paymentName;

  HistoryTransaction({
    @required this.id,
    @required this.nameAction,
    @required this.cost,
    @required this.image,
    @required this.paymentName
  });
}