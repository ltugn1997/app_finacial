import 'package:flutter/material.dart';

class SubApp {
  final String id;
  final String name;
  final String logo;
  final double monthlyCost;

  SubApp({
    @required this.id,
    @required this.name,
    @required this.logo,
    @required this.monthlyCost,
  });
}