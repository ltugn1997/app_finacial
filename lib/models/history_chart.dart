import 'package:flutter/material.dart';

class HistoryChartModel{
  final double totalSpendMoney;
  final List<ChartItem> chartItems;

  HistoryChartModel({
    @required this.totalSpendMoney,
    @required this.chartItems,
  });
}

class ChartItem{
  String name;
  double percent;
  double value;
  Color color;

  ChartItem({
    @required this.name,
    @required this.percent,
    @required this.value,
    @required this.color,
  });
}