import 'package:financeappui/providers/user_auth.dart';
import 'package:financeappui/datas/friends.dart';
import 'package:financeappui/datas/payment_method.dart';
import 'package:financeappui/datas/subscriptions.dart';
import 'package:financeappui/helper/total_balance_user.dart';
import 'package:flutter/material.dart';

List<AuthUser> authUsers = [
  AuthUser(userName: 'demo', 
  gmail:'demo@gmail.com', 
  passWord: 'demo123', 
  phone: '0961325198', 
  id: '1',
  profileImage:'assets/images/myprofile.png',
  totalBalance: totalBalanceUser(paymentMethods),
  friends: friendsList,
  subApps: subscriptionsList,
  histories: [],
  paymentMethods: paymentMethods,
  ),
];


class Contants{
  static OutlineInputBorder border = OutlineInputBorder(
    borderRadius: BorderRadius.circular(30),
    borderSide: BorderSide(color: Colors.transparent),
  );
}
