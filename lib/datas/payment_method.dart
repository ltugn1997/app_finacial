import 'package:financeappui/models/payment_menthod.dart';

List<PaymentMethod> paymentMethods = [
  PaymentMethod(id: '1', methodName: 'MasterCard', logo: 'assets/images/mastercard.png', totalBalance: 2000, infoMethod: '123456789'),
  PaymentMethod(id: '2', methodName: 'PayPal', logo: 'assets/images/paypal.png', totalBalance: 1000, infoMethod: '123456789'), 
  PaymentMethod(id: '3', methodName: 'Visa', logo: 'assets/images/visa.png', totalBalance: 1500, infoMethod: '123456789'), 
];
