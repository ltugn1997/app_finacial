import 'package:financeappui/models/sub_app.dart';

List<SubApp> subscriptionsList = [
  SubApp(id:'1', name: 'Netflix', monthlyCost: 9.90, logo: 'assets/images/netflix-logo.png'),
  SubApp(id:'2', name: 'Dropbox', monthlyCost: 19.90, logo: 'assets/images/dropbox-logo.png'),
  SubApp(id:'3', name: 'Spotify', monthlyCost: 6.90, logo: 'assets/images/spotify-logo.png'),
];
