import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const navigationBarRoutes = ['/', '/add_friend', '/setting_screen'];

tabItems (context) => [
  BottomNavigationBarItem(
    icon: Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Image.asset(
        'assets/images/home-icon.png',
        width: 22,
      ),
    ),
    activeIcon: Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Image.asset(
        'assets/images/home-icon.png',
        width: 22,
        color: Theme.of(context).primaryColor,
      ),
    ),
    title: Text(''),
  ),
  BottomNavigationBarItem(
    icon: Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Image.asset(
        'assets/images/plus-icon.png',
        width: 22,
      ),
    ),
    activeIcon: Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Image.asset(
        'assets/images/plus-icon.png',
        color: Theme.of(context).primaryColor,
        width: 22,
      ),
    ),
    title: Text(''),
  ),
  BottomNavigationBarItem(
    icon: Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Image.asset(
        'assets/images/setting.png',
        width: 22,
      ),
    ),
    activeIcon: Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Image.asset(
        'assets/images/setting.png',
        color: Theme.of(context).primaryColor,
        width: 22,
      ),
    ),
    title: Text(''),
  ),
];

BottomNavigationBar generateBottomNavigationBar(context, currentIndex) {
  return BottomNavigationBar(
    items: tabItems(context),
    onTap: (index) {
      if (index != currentIndex) {
        Navigator.pushReplacementNamed(context, navigationBarRoutes[index]);
      }
    },
    type: BottomNavigationBarType.fixed,
    backgroundColor: Colors.white,
    selectedItemColor: Color(0xFF3D8DFB),
    currentIndex: currentIndex,
    selectedFontSize: 12.0,
  );
}