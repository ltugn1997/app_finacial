import 'package:financeappui/helper/check_input.dart';
import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  String hint;
  bool issecured;
  String type;
  TextEditingController myController;
  TextInputType textInputType;
  Function(String) onchange;

  CustomTextField(this.myController, {this.hint, this.issecured, this.type, this.textInputType, this.onchange,});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(left: 25, right: 25),
        child: TextFormField(
        obscureText: issecured,
        keyboardType: textInputType,
        controller: myController,
          cursorColor: Colors.black,
          style: TextStyle(
                  fontSize: 18,
                  letterSpacing: 1.5,
                  color: Colors.grey[800],
                  fontWeight: FontWeight.w900),
          validator: (String value){
            if(value.isEmpty){
              return'please provide a value';
            }
            if(type == 'Gmail' && !validateEmail(value)){
              return 'Please enter valid email (ex: example@gmail.com)';
            }
            // if(type == 'Password' && !validatePassword(value)){
            //   return ' A password must be eight characters including\n one uppercase letter,\n one special character and alphanumeric characters.';
            // }
            return null;
          },
          onChanged: onchange ?? null,
          decoration: InputDecoration(
            contentPadding:
            EdgeInsets.all(20),
              disabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.black),
                borderRadius: BorderRadius.circular(25),
              ),
              hintText: hint,

              hintStyle: TextStyle(
                  fontSize: 18,
                  letterSpacing: 1.5,
                  color: Colors.grey[800],
                  fontWeight: FontWeight.w900),
              filled: true,
              hoverColor: Colors.transparent,
              focusColor: Colors.transparent,
              fillColor: Colors.white.withOpacity(.3),
              border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.black),
                borderRadius: BorderRadius.circular(25),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.black),
                borderRadius: BorderRadius.circular(25),
              )),
        ));
  }
}
