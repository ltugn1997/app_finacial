import 'package:financeappui/models/payment_menthod.dart';
import 'package:financeappui/providers/user_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

Widget paymentMethodDropDown(){
  return Consumer<AuthUsers>(
    builder: (context, authUser, child) {
      return Center(
        child: Container(
          width: MediaQuery.of(context).size.width  * 0.9,
          padding: EdgeInsets.all(15),
          decoration: BoxDecoration(
              border: Border.all(width: 1, color: Colors.grey),
              borderRadius: BorderRadius.circular(10)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: DropdownButtonHideUnderline(
                  child: ButtonTheme(
                    alignedDropdown: true,
                    child: DropdownButton<PaymentMethod>(
                      isDense: true,
                      hint: Text("Select Bank"),
                      value: authUser?.selectedUser?.selectedPayment,
                      onChanged: (PaymentMethod newValue) {
                        authUser.getSelectedPayment(newValue);      
                      },
                      items: authUser.selectedUser.paymentMethods.map((PaymentMethod paymentMethod) {
                        return DropdownMenuItem<PaymentMethod>(
                          value: paymentMethod,
                          child: Row(
                            children: <Widget>[
                              Image.asset(
                                paymentMethod.logo,
                                width: 25,
                              ),
                              Container(
                                  margin: EdgeInsets.only(left: 10),
                                  child: Text(paymentMethod.methodName)),
                            ],
                          ),
                        );
                      }).toList(),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
     }
  );
}