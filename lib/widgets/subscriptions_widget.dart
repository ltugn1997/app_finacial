import 'package:financeappui/datas/subscriptions.dart';
import 'package:financeappui/helper/show_dialog.dart';
import 'package:financeappui/models/sub_app.dart';
import 'package:financeappui/providers/user_auth.dart';
import 'package:financeappui/widgets/payment_method.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SubscriptionsWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.25,
      width: MediaQuery.of(context).size.width,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        physics: BouncingScrollPhysics(),
        itemCount: subscriptionsList.length,
        itemBuilder: (context, index) {
          SubApp item = subscriptionsList[index];
          return GestureDetector(
            onTap: (){showBottomSheet(context, size, item);},
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 24.0, top: 15.0),
                  child: Container(
                    width: size.width * 0.3,
                    height: size.height * 0.23,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(12),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.01),
                            spreadRadius: 0.001,
                            blurRadius: 10,
                            offset: Offset(0, 0),
                          )
                        ],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 14.0, top: 14),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                height: 30,
                                child: Image.asset(item.logo),
                              ),
                              SizedBox(height: 10),
                              Text(
                                item.name,
                                style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 12,
                                ),
                              )
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 10.0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  '\$${item.monthlyCost}',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 17,
                                  ),
                                ),
                                Text(
                                  '/mo',
                                  style: TextStyle(
                                    color: Colors.black.withOpacity(0.3),
                                    fontWeight: FontWeight.w700,
                                    fontSize: 13,
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            )
          );
        }
      ),
    );
  }
}

void showBottomSheet(BuildContext context, Size size, SubApp item){
    showModalBottomSheet(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
        top: Radius.circular(25),
      )),
      isScrollControlled: true,
      backgroundColor: Colors.white,
      enableDrag: true,
      barrierColor: Colors.black.withOpacity(0.2),
      context: context,
      builder: (BuildContext cnt) {
        return Container(
          height: size.height * 0.4,
          child: Column(children: [
            Container(
              padding: EdgeInsets.all(20),
              child: Text('Select a payment method',
                style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 25,
              ),),
            ),
            SizedBox(height: 10,),
            Consumer<AuthUsers>(
                builder: (context, authUser, child) {
                  String name = authUser?.selectedUser?.selectedPayment?.methodName ?? '';
                  return  Container(
                    padding: EdgeInsets.only(right: 20),
                    child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        'Total Balance $name: ',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w700,
                          fontSize: 20,
                        ),
                      ),
                      Text(
                        '\$${authUser?.selectedUser?.selectedPayment?.totalBalance?.toStringAsFixed(1) ?? 0}',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w700,
                          fontSize: 20,
                        ),
                      ),
                    ],
                  )
                );
              },
            ),
            paymentMethodDropDown(),
            buttonSendMoney(item),
          ],),
        );
      }
    ).whenComplete((){
  });
}

Widget buttonSendMoney(SubApp item){
  return  Consumer<AuthUsers>(
    builder: (context, authUser, child) {
      return  InkWell(
        onTap: (){
          sendMoneyToFriend(authUser, context, item);
        },
        child: Padding(
          padding: const EdgeInsets.only(
              left: 20.0, right: 20.0, top: 40),
          child: Container(
            height: 56,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: Colors.black,
            ),
            child: Center(
              child: Text(
                'Send Money',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w700,
                  fontSize: 16,
                ),
              ),
            ),
          ),
        ),
      );
    }
  );
}

void sendMoneyToFriend(AuthUsers authUser, BuildContext context, SubApp item){

  if(authUser?.selectedUser?.selectedPayment != null){
    authUser.updateMoney(item.monthlyCost, context, item.name, item.logo, authUser.selectedUser.selectedPayment.methodName);
    return;
  }
  showMessage(context, 'Failed Transaction', 'You have not selected a payment method', 'Please, Check Again');
}