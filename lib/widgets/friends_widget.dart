import 'package:financeappui/helper/check_input.dart';
import 'package:financeappui/helper/show_dialog.dart';
import 'package:financeappui/providers/user_auth.dart';
import 'package:financeappui/models/friend.dart';
import 'package:financeappui/widgets/custom_text_field.dart';
import 'package:financeappui/widgets/payment_method.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FriendsMoneyWidget extends StatefulWidget {
  FriendsMoneyWidget({Key key}) : super(key: key);

  @override
  _FriendsMoneyWidgetState createState() => _FriendsMoneyWidgetState();
}

class _FriendsMoneyWidgetState extends State<FriendsMoneyWidget> {
   TextEditingController moneyController =  TextEditingController();
  void showBottomSheet(BuildContext context, Friend friendIndex, Size size, AuthUser authUser){
    showModalBottomSheet(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
        top: Radius.circular(25),
      )),
      isScrollControlled: true,
      backgroundColor: Colors.white,
      enableDrag: true,
      barrierColor: Colors.black.withOpacity(0.2),
      context: context,
      builder: (BuildContext cnt) {
        return Container(
          height: size.height * 0.6,
          child: Wrap(
            children: [
              Padding(
                padding: const EdgeInsets.only(
                  left: 20.0, right: 20.0, top: 40),
                child: Row(
                  mainAxisAlignment:
                    MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Image.asset(
                          friendIndex.image,
                          width: 48,
                        ),
                        SizedBox(width: 20),
                        Text(
                          friendIndex.name,
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 14,
                          ),
                        )
                      ],
                    ),
                    Consumer<AuthUsers>(
                      builder: (context, authUser, child) {
                        return  Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            '\$ ${authUser?.selectedUser?.selectedPayment?.totalBalance?.toStringAsFixed(1) ?? 0}',
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w700,
                              fontSize: 20,
                            ),
                          ),
                          Text(
                            'Comission: 1\$',
                            style: TextStyle(
                              color: Colors.black.withOpacity(0.3),
                              fontWeight: FontWeight.w500,
                              fontSize: 12,
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                  ],
                ),
              ),
               Consumer<AuthUsers>(
                builder: (context, authUser, child) {
                return Container(
                  padding: EdgeInsets.symmetric(vertical: 20),
                  child: CustomTextField(
                      moneyController,
                      hint: 'Enter Your Money',
                      issecured: false,
                      textInputType: TextInputType.number
                    ),
                  );
                }
              ),
              paymentMethodDropDown(),
              buttonSendMoney(friendIndex),
            ],
          ),
        );
      }
    ).whenComplete((){
      moneyController.text = '';
    });
  }

Widget buttonSendMoney(Friend friendIndex){
  return  Consumer<AuthUsers>(
    builder: (context, authUser, child) {
      return  InkWell(
        onTap: (){
          sendMoneyToFriend(authUser, context, friendIndex);
        },
        child: Padding(
          padding: const EdgeInsets.only(
              left: 20.0, right: 20.0, top: 40),
          child: Container(
            height: 56,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: Colors.black,
            ),
            child: Center(
              child: Text(
                'Send Money',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w700,
                  fontSize: 16,
                ),
              ),
            ),
          ),
        ),
      );
    }
  );
}

void sendMoneyToFriend(AuthUsers authUser, BuildContext context, Friend friendIndex){
  if(!checkInput(moneyController.text, context)){
    return;
  }
  if(authUser?.selectedUser?.selectedPayment != null){
    authUser.updateMoney(convertToDouble(moneyController.text), context, friendIndex.name, friendIndex.image, authUser.selectedUser.selectedPayment.methodName);
    return;
  }
  showMessage(context, 'Failed Transaction', 'You have not selected a payment method', 'Please, Check Again');
}

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final AuthUsers authUsers = Provider.of<AuthUsers>(context, listen: false);
    final AuthUser authUser = authUsers.selectedUser;
    return Container(
      height: size.height * 0.12,
      width: MediaQuery.of(context).size.width,
      child: ListView.builder(
        itemCount: authUser.friends.length,
        scrollDirection: Axis.horizontal,
        physics: BouncingScrollPhysics(),
        itemBuilder: (context, index) {
          var friendIndex = authUser.friends[index];
          return GestureDetector(
            onTap: () {
              showBottomSheet(context, friendIndex, size, authUser);
            },
            child: Padding(
              padding: const EdgeInsets.only(left: 24.0, top: 15),
              child: Column(
                children: [
                  Image.asset(
                    friendIndex.image,
                    width: 56,
                  ),
                  SizedBox(height: 10),
                  Text(
                    friendIndex.name,
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 12,
                    ),
                  )
                ],
              ),
            ),
          );
        }
      ),
    );
  }
}