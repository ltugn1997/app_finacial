import 'package:financeappui/providers/user_auth.dart';
import 'package:financeappui/widgets/main_chart_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


class MyProfileWidget extends StatefulWidget {
  MyProfileWidget({Key key}) : super(key: key);

  @override
  MyProfileWidgetState createState() => MyProfileWidgetState();
}

class MyProfileWidgetState extends State<MyProfileWidget> {

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 24.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Consumer<AuthUsers>(
            builder: (context, authUser, child) {
              return Image.asset(
                authUser.selectedUser.profileImage,
                width: 61,
              );
            }
          ),
          MainChart(),
          Text(
            'Send money',
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 16,
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }
}
