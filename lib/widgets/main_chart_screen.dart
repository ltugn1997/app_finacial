import 'package:financeappui/models/history_chart.dart';
import 'package:financeappui/providers/user_auth.dart';
import 'package:financeappui/widgets/indicator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class MainChart extends StatefulWidget {
  MainChart({Key key}) : super(key: key);

  @override
  _MainChartState createState() => _MainChartState();
}

class _MainChartState extends State<MainChart> {

  List<charts.Series<ChartItem, String>> _pieData;

  generateData(AuthUsers authUser){
  List<ChartItem> piedata = authUser.selectedUser.paymentMenthodChart.chartPaymentMenthod;
  _pieData = [];
  _pieData.add(
    charts.Series(
      domainFn: (ChartItem data, _) => data.name +" ${data.percent.toStringAsFixed(0)}%",
      measureFn: (ChartItem data, _) => data.percent,
      colorFn: (ChartItem data, _) => charts.ColorUtil.fromDartColor(data.color),
      id: 'Money Spend',
      data: piedata,
      labelAccessorFn: (ChartItem data, _) => '\$${data.value.toStringAsFixed(0)}',
    ),
  );
  return _pieData;
  }

  List<Widget> noteWidget(AuthUsers authUser){
    List<ChartItem> chartItems = authUser.selectedUser.paymentMenthodChart.chartPaymentMenthod;
    List<Widget> indicator = [];
    for(ChartItem chartItem in chartItems){
        Widget indicatorRender = Indicator(
          color: chartItem.color,
          text: chartItem.name,
          isSquare: true,
        );
        indicator.add(indicatorRender);
    }
    return indicator;
  }
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      width: 400,
      height: 300,
      child: Row(
          children: [
            Expanded(
              child: Consumer<AuthUsers>(
                builder: (context, authUser, child) {
                  return charts.PieChart(
                    generateData(authUser),
                    defaultRenderer: charts.ArcRendererConfig(
                      arcWidth: 60,
                      arcRendererDecorators: [
                      charts.ArcLabelDecorator(
                      labelPosition: charts.ArcLabelPosition.inside,
                      insideLabelStyleSpec: charts.TextStyleSpec(fontSize: 18, color: charts.Color.black))
                    ]
                  ),
                );
              }
            ),),
            Container(
              margin: EdgeInsets.only(right: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                Consumer<AuthUsers>(
                builder: (context, authUser, child) {
                  return Container(
                    height: size.height * 0.1,
                    child: Wrap(
                      spacing: 5,
                      runSpacing: 10,
                      direction: Axis. vertical,
                      children: noteWidget(authUser)
                    ),
                  );
                }
              ),
            ]),
          ),
        ],
      )
    );
  }
}