import 'package:financeappui/providers/user_auth.dart';
import 'package:flutter/material.dart';

Widget buttonSendMoney(Function sendMoneyToFriend, AuthUsers authUser, BuildContext context, dynamic receiver){
  return  InkWell(
    onTap: (){
      sendMoneyToFriend(authUser, context, receiver);
    },
    child: Padding(
      padding: const EdgeInsets.only(
          left: 20.0, right: 20.0, top: 40),
      child: Container(
        height: 56,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: Colors.black,
        ),
        child: Center(
          child: Text(
            'Send Money',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w700,
              fontSize: 16,
            ),
          ),
        ),
      ),
    ),
  );
}