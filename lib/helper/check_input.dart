import 'package:financeappui/helper/show_dialog.dart';
import 'package:financeappui/models/friend.dart';
import 'package:flutter/material.dart';

bool validateEmail(dynamic value) {
  const Pattern pattern = ValidatePatern.email;
  final RegExp regex = RegExp(pattern);
  if (!regex.hasMatch(value)) {
    return false;
  }
  return true;
}
bool validatePassword(dynamic value) {
  const Pattern pattern = ValidatePatern.password;
  final RegExp regex = RegExp(pattern);
  if (!regex.hasMatch(value)) {
    return false;
  }
  return true;
}
class ValidatePatern{
  static const Pattern email = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  static const Pattern password = r'^((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W]).{7,64})$';
}

bool checkMoney(num withdraw, num moneyInAccount) {
  if( withdraw > moneyInAccount){
    return false;
  }
  return true;
}

double convertToDouble(String string){
  if(string == ""){
    return 0;
  }
  return double.parse(string);
}

bool checkInput(String money, BuildContext context){
  if(money.isEmpty){
    showMessage(context, 'Failed Transaction', 'You have not entered the amount to transfer', 'Please, Check Again');
    return false;
  }
  if(convertToDouble(money) <= 0){
    showMessage(context, "Wrong Input", "Input must be a positive number", 'Please, Check Again');
    return false;
  }
  return true;

}

bool checkFriend(Friend newfriend, List<Friend> friends){
  for(Friend friend in friends){
    if(newfriend.id == friend.id){
      return false;
    }
  }
  return true;
}