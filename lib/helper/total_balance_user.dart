import 'package:financeappui/models/history_chart.dart';
import 'package:financeappui/models/history_transaction.dart';
import 'package:financeappui/models/payment_menthod.dart';
import 'package:flutter/material.dart';

double totalBalanceUser(List<PaymentMethod>paymentMethods){
  double totalMoney = 0;
  for(PaymentMethod paymentMethod in paymentMethods){
    totalMoney += paymentMethod.totalBalance ;
  }
  return totalMoney;
}

double totalSpendMoney(List<HistoryTransaction> histories){
  double total = 0;
  for(HistoryTransaction historyTransaction in histories)
  {
    total += historyTransaction.cost;
  }
  return (total).abs();
}

Map<String, double> convertChartItemsMap(List<HistoryTransaction> histories){
  Map<String, double> activity = {};
  for(HistoryTransaction historyTransaction in histories)
  {
    if (activity.containsKey(historyTransaction.nameAction)){
      activity[historyTransaction.nameAction] = (activity[historyTransaction.nameAction]).abs() + (historyTransaction.cost).abs();
    }
    if (! activity.containsKey(historyTransaction.nameAction)){
      activity[historyTransaction.nameAction] = (historyTransaction.cost).abs();
    }
  }
  return activity;
}

Map<String, double> convertChartPaymentMenthodMap(List<PaymentMethod> paymentMethods){
  Map<String, double> activity = {};
  for(PaymentMethod paymentMethod in paymentMethods)
  {
    if (activity.containsKey(paymentMethod.methodName)){
      activity[paymentMethod.methodName] = (activity[paymentMethod.methodName]).abs() + (paymentMethod.totalBalance).abs();
    }
    if (! activity.containsKey(paymentMethod.methodName)){
      activity[paymentMethod.methodName] = (paymentMethod.totalBalance).abs();
    }
  }
  return activity;
}


List<ChartItem> updateChartItems(Map<String, double> activity, double total, {String type}){
  List<ChartItem> chartItems = [];
  for(String key in activity.keys)
  {
    ChartItem chartItem = ChartItem(
      value: activity[key],
      percent: (activity[key]/total) * 100,
      name: key,
      color: checkColor(key),
      );
    chartItems.add(chartItem);
  }
  return chartItems;
}

Color checkColor(String name){
  switch (name) {
    case 'MasterCard': {
      return Colors.red[700];
      //return Colors.orange[800];
    }
    case 'PayPal': {
      return Colors.blue[600];
    }
    case 'Visa': {
      return Colors.green;
      //return Colors.teal;
    }
    case 'Jennifer': {
      return Colors.pink[900];
    }
    case 'Michael': {
      return Colors.yellow[900];
    }
    case 'Ellie': {
      return Colors.brown;
    }
    case 'Paul': {
      return Colors.brown;
    }
    case 'Diana': {
      return Colors.cyan;
    }
    case 'Netflix': {
      return Colors.red[900];
    }
    case 'Dropbox': {
      return Colors.blueGrey[700];
    }
    case 'Spotify': {
      return Colors.greenAccent;
    }
    default: {
      return Colors.grey;
    }
  }
}