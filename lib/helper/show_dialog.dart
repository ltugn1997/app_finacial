import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void showMessage(BuildContext context, title, header, buttonName, {Widget buttonTwo}){
  showDialog(
    context: context,
    builder: (_) => CupertinoAlertDialog(
      title: Text(title),
      content: Text(header),
      actions: <Widget>[
        Container(
          child: FlatButton(
            child: Text(buttonName),
            onPressed: () {
              Navigator.of(context).pop();
            },
          )
        ),
      ],
    )
  );
}

void showMessageTwoOption(BuildContext context, title, header, buttonNameOne, buttonNameTwo ){
  showDialog(
    context: context,
    builder: (_) => CupertinoAlertDialog(
      title: Text(title),
      content: Text(header),
      actions: <Widget>[
        Container(
          child: FlatButton(
            child: Text(buttonNameOne),
            onPressed: () {
              Navigator.of(context).pop();
            },
          )
        ),
        Container(
          child: FlatButton(
            child: Text(buttonNameTwo),
            onPressed: () {
              Navigator.of(context).pop();
            },
          )
        ),
      ],
    )
  );
}