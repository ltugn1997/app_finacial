import 'package:financeappui/datas/auth_users.dart';
import 'package:financeappui/datas/payment_method.dart';
import 'package:financeappui/helper/check_input.dart';
import 'package:financeappui/helper/show_dialog.dart';
import 'package:financeappui/helper/total_balance_user.dart';
import 'package:financeappui/models/friend.dart';
import 'package:financeappui/models/history_chart.dart';
import 'package:financeappui/models/history_transaction.dart';
import 'package:financeappui/models/payment_menthod.dart';
import 'package:financeappui/models/payment_menthod_chart.dart';
import 'package:financeappui/models/sub_app.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AuthUser{
  final String userName;
  final String gmail;
  final String passWord;
  final String phone;
  final String id;
  final String profileImage;
  final double totalBalance;
  final List<Friend> friends;
  final List<SubApp> subApps;
  final List<HistoryTransaction> histories;
  final List <PaymentMethod> paymentMethods;
  final PaymentMethod selectedPayment;
  final double money;
  final HistoryChartModel historyChart;
  final PaymentMenthodChart paymentMenthodChart;

  AuthUser({
    this.userName,
    this.gmail,
    this.passWord,
    this.phone,
    this.id,
    this.profileImage,
    this.totalBalance,
    this.friends,
    this.subApps,
    this.histories,
    this.paymentMethods,
    this.selectedPayment,
    this.money,
    this.historyChart,
    this.paymentMenthodChart
  });

  AuthUser copyWith({
    final String userName,
    final String gmail,
    final String passWord,
    final String phone,
    final String id,
    final String profileImage,
    final double totalBalance,
    final List<Friend> friends,
    final List<SubApp> subApps,
    final List<HistoryTransaction> histories,
    final List <PaymentMethod> paymentMethods,
    final PaymentMethod selectedPayment,
    final double money,
    final HistoryChartModel historyChart,
    final PaymentMenthodChart paymentMenthodChart,
  }) {
    return AuthUser(
      userName: userName ?? this.userName,
      gmail: gmail ?? this.gmail,
      passWord: passWord ?? this.passWord,
      phone: phone ?? this.phone,
      id: id ?? this.id,
      profileImage: profileImage ?? this.profileImage,
      totalBalance: totalBalance ?? this.totalBalance,
      friends: friends ?? this.friends,
      subApps: subApps ?? this.subApps,
      histories: histories ?? this.histories,
      paymentMethods: paymentMethods ?? this.paymentMethods,
      selectedPayment: selectedPayment ?? this.selectedPayment,
      money: money ?? this.money ?? 0,
      historyChart: historyChart ?? this.historyChart ?? null,
      paymentMenthodChart: paymentMenthodChart ?? this.paymentMenthodChart ?? null,
    );
  }
}

class AuthUsers with ChangeNotifier{
  List<AuthUser> _items = authUsers;
  AuthUser selectedUser = authUsers[0];

  List<AuthUser> get items{
    return [... _items];
  }

  void getCurrentUser(String id){
    List<AuthUser> currentUserList = items.where((AuthUser authUser) => authUser.id == id).toList();
    if (currentUserList.isEmpty){
      selectedUser = authUsers[0];
      notifyListeners();
    }
    if (currentUserList.isNotEmpty){
      selectedUser = currentUserList[0];
      notifyListeners();
    }
  }

  void addUser(String gmail, String passWord, String userName, String phone) {
    _items.insert(
      0,
      AuthUser(userName: userName,
        passWord: passWord,
        gmail: gmail,
        phone: phone,
        id: DateTime.now().toString(),
        profileImage: 'assets/images/defaul_avatar.png',
        totalBalance: totalBalanceUser(paymentMethods),
        friends: [],
        subApps: [],
        histories: [],
        paymentMethods: paymentMethods,
        selectedPayment: null,
        money: 0,
        )
    );
    notifyListeners();
  }

  void editUser(String id, String gmail, String passWord, String userName, String phone){
    AuthUser oldSelectedUser = _items.firstWhere((AuthUser authUser) => authUser.id == id);
    AuthUser newSelectedUser  = AuthUser(
      id: oldSelectedUser.id,
      gmail: gmail ?? oldSelectedUser.gmail,
      passWord: passWord ?? oldSelectedUser.passWord,
      userName: userName ?? oldSelectedUser.userName,
      phone: phone ?? oldSelectedUser.phone,
    );

    _items.remove(oldSelectedUser);
    _items.add(newSelectedUser);
    selectedUser = newSelectedUser;

    notifyListeners();
  }

  void addFriend(String phone, BuildContext context) {
    AuthUser ownerPhone = _items.firstWhere((AuthUser authUser) =>  authUser.phone == phone, orElse: () => null);
    if(ownerPhone == null){
      showMessageTwoOption(context, "Confirm", "This phone number have not activated yet.\n Do you want to invite this person to use financial application?", 'No', 'yes');
      return;
    }

    if(ownerPhone.id == selectedUser.id){
      showMessage(context, "Something Wrong", "You can not make friends with yourself", 'Please, Check Again');
      return;
    }

    Friend newFriend = Friend(id: DateTime.now().toString(), name: ownerPhone.userName, image: ownerPhone.profileImage);
    if(!checkFriend(newFriend, selectedUser.friends)){
      showMessage(context, "Something Wrong", "You have already friend of the phone number owner", 'Please, Check Again');
      return;
    }
    selectedUser.friends.insert(
      0, newFriend
    );
    showDialog(
      context: context,
      builder: (_) => CupertinoAlertDialog(
        title: Text("Make Friend Success"),
      )
    );
    notifyListeners();
  }


  void getSelectedPayment(PaymentMethod selectedMethod){
    selectedUser = selectedUser.copyWith(selectedPayment: selectedMethod);
    notifyListeners();
  }

  void addHistory(String id, String receiverName, double cost, String receiverImage, String paymentName) {
    HistoryTransaction histories = HistoryTransaction(id: id, cost: cost, nameAction: receiverName, image: receiverImage, paymentName: paymentName);
    selectedUser.histories.insert( 0,histories);
    notifyListeners();
  }

  void addHistoryChart() {
    List<HistoryTransaction> histories = selectedUser.histories;
    double total = totalSpendMoney(histories);
    Map<String, double> chartItemsMap = convertChartItemsMap(histories);
    List<ChartItem> chartItemsRender = updateChartItems(chartItemsMap, total);
    HistoryChartModel historyChart = HistoryChartModel(totalSpendMoney: total, chartItems: chartItemsRender);
    selectedUser = selectedUser.copyWith(historyChart: historyChart);
    notifyListeners();
  }

  void addPaymentMenthodChart() {
    List<PaymentMethod> paymentMenthods= selectedUser.paymentMethods;
    double total = selectedUser.totalBalance;
    Map<String, double> chartItemsMap = convertChartPaymentMenthodMap(paymentMenthods);
    List<ChartItem> chartItemsRender = updateChartItems(chartItemsMap, total, type: 'payment');
    PaymentMenthodChart paymentMenthodChart = PaymentMenthodChart(totalSpendMoney: total, chartPaymentMenthod: chartItemsRender);
    selectedUser = selectedUser.copyWith(paymentMenthodChart: paymentMenthodChart);
  }

  void updateMoney(double money, BuildContext context, String receiverName, String receiverImage, String paymentName){
    bool check = checkMoney(money, selectedUser.selectedPayment.totalBalance);
    if (check){
      double updateMoney = selectedUser.selectedPayment.totalBalance - money;
      PaymentMethod newSelectedPayment = PaymentMethod(
      id: selectedUser.selectedPayment.id,
      methodName: selectedUser.selectedPayment.methodName,
      totalBalance: updateMoney,
      logo: selectedUser.selectedPayment.logo,
      infoMethod: selectedUser.selectedPayment.infoMethod,
      );
      List<PaymentMethod> newpaymentMethods = [];
      for( PaymentMethod oldSelectedPayment in selectedUser.paymentMethods){
        if(oldSelectedPayment.id == newSelectedPayment.id){
          newpaymentMethods.add(newSelectedPayment);
          continue;
        }
        newpaymentMethods.add(oldSelectedPayment);
      }

      selectedUser = selectedUser.copyWith(
        selectedPayment: newSelectedPayment,
        paymentMethods: newpaymentMethods,
        totalBalance: totalBalanceUser(newpaymentMethods));

      addHistory(DateTime.now().toString(), receiverName, money * -1, receiverImage, paymentName);

      notifyListeners();
      showDialog(
        context: context,
        builder: (_) => CupertinoAlertDialog(
          title: Text("Transaction Success"),
        )
      );
      return;
    }
    showMessage(context, "Failed Transaction", "The money in the account is not enough", 'Please, Check Again');
  }
}


