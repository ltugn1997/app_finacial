import 'package:financeappui/providers/user_auth.dart';
import 'package:financeappui/widgets/bottom_navigation_bar.dart';
import 'package:financeappui/widgets/custom_text_field.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddFriendScreen extends StatefulWidget {
  AddFriendScreen({Key key}) : super(key: key);

  @override
  _AddFriendScreenState createState() => _AddFriendScreenState();
}

class _AddFriendScreenState extends State<AddFriendScreen> {
   TextEditingController phoneController =  TextEditingController();
   final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: generateBottomNavigationBar(context, 1),
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          backgroundColor: Colors.transparent,
          title: Text("Add Friend With Phone", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),),
          leading: Align(
            alignment: Alignment.center,
            child: IconButton(
              color: Colors.black,
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () {
              Navigator.pushReplacementNamed(context, '/');
            }),
          ),
          ),
        body: Form(
          key: _formKey,
          child: Consumer<AuthUsers>(
            builder: (context, authUser, child) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children:[
                  CustomTextField(
                    phoneController,
                    hint: '    enter your Phone',
                    issecured: false,
                    textInputType: TextInputType.number,
                    onchange: (_){setState((){});},
                  ),

                  buttonMakeFriend(makeFriend, authUser, phoneController.text, _formKey, context),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}

void makeFriend(AuthUsers authUser, String phone, _formKey, BuildContext context){
  bool isValidType = _formKey.currentState.validate();
  if(isValidType){
    authUser.addFriend(phone, context);
  }
}

Widget buttonMakeFriend(Function makeFriend, AuthUsers authUser, String phone, _formKey, BuildContext context){
  return GestureDetector(
    onTap: (){makeFriend(authUser, phone, _formKey, context);},
  child: Padding(
    padding: const EdgeInsets.only(
        left: 20.0, right: 20.0, top: 40),
    child: Container(
      height: 56,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        color: Colors.black,
      ),
      child: Center(
        child: Text(
          'Search',
          style: TextStyle(
            fontFamily: 'Avenir',
            color: Colors.white,
            fontWeight: FontWeight.w700,
            fontSize: 16,
          ),
        ),
      ),),
    ),
  );
}