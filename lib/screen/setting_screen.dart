import 'package:financeappui/providers/user_auth.dart';
import 'package:financeappui/widgets/bottom_navigation_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SettingScreen extends StatefulWidget {
  SettingScreen({Key key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<SettingScreen> {
  @override
  Widget build(BuildContext context) {
    bool isvalidHistories = Provider.of<AuthUsers>(context, listen: false).selectedUser.histories.isEmpty;
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: generateBottomNavigationBar(context, 2),
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          title: Text('Setting', style: TextStyle(color: Colors.black),),),
        body:Column(children: [
          ListTile(
            leading: Icon(Icons.arrow_back),
            title: Text('Logout', style: TextStyle(
              fontSize: 18
            ),),
            onTap: (){
              Navigator.of(context).pushReplacementNamed('/login_screen');
            }
          ),
          ListTile(
            leading: Icon(Icons.history),
            title: Text('History', style: TextStyle(
              fontSize: 18
            ),),
            onTap: (){
              if (isvalidHistories){
                showDialog(
                  context: context,
                  builder: (_) => CupertinoAlertDialog(
                    title: Text('You have not had any transactions'),
                  )
                );
              }
              if (!isvalidHistories){
                Navigator.of(context).pushNamed('/history_screen');
              }
            }
          ),
          ListTile(
            leading: Icon(Icons.history),
            title: Text('History Chart', style: TextStyle(
              fontSize: 18
            ),),
            onTap: (){
              if (isvalidHistories){
                showDialog(
                  context: context,
                  builder: (_) => CupertinoAlertDialog(
                    title: Text('You have not had any transactions'),
                  )
                );
              }
              if (!isvalidHistories){
                Provider.of<AuthUsers>(context, listen: false).addHistoryChart();
                Navigator.of(context).pushNamed('/history_chart');
              }
            }
          ),
        ],)
      )
    );
  }
}