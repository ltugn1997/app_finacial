import 'package:financeappui/providers/user_auth.dart';
import 'package:financeappui/widgets/bottom_navigation_bar.dart';
import 'package:financeappui/widgets/friends_widget.dart';
import 'package:financeappui/widgets/my_profile_widget.dart';
import 'package:financeappui/widgets/subscriptions_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  Future<void> didChangeDependencies(){
    AuthUsers authUsers = Provider.of<AuthUsers>(context, listen: true);
    authUsers.addPaymentMenthodChart();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: generateBottomNavigationBar(context, 0),
      body: SafeArea(
        child: ListView(
          children: [
            SizedBox(height: 10),
            MyProfileWidget(),
            FriendsMoneyWidget(),
            SizedBox(height: 10),
            textAvenir('Subscriptions'),
            SubscriptionsWidget(),
          ],
        ),
      ),
    );
  }
}

Widget textAvenir(String text){
  return Padding(
    padding: const EdgeInsets.only(left: 24.0),
    child: Text(text,
      style: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: 16,
        color: Colors.black,
      ),
    ),
  );
}