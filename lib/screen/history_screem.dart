import 'package:financeappui/models/history_transaction.dart';
import 'package:financeappui/providers/user_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

class HistoryScreen extends StatefulWidget {
  HistoryScreen({Key key}) : super(key: key);

  @override
  _HistoryScreenState createState() => _HistoryScreenState();
}

class _HistoryScreenState extends State<HistoryScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          backgroundColor: Colors.transparent,
          title: Text("Transaction History", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),),
          leading: Align(
            alignment: Alignment.center,
            child: IconButton(
              color: Colors.black,
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () {
              Navigator.pop(context);
            }),
          ),
          ),
        body: Consumer<AuthUsers>(
          builder: (context, authUser, child) {
            return ListView.builder(
          itemCount: authUser.selectedUser.histories.length,
          physics: BouncingScrollPhysics(),
          itemBuilder: (context, index) {
            HistoryTransaction historyIndex = authUser.selectedUser.histories[index];
            return Card(
              child: historyCard(historyIndex)
            );
            }
          );
          },
        ),
      ),
    );
  }
}

Widget historyCard(HistoryTransaction historyIndex){
  return Container(
    margin: EdgeInsets.all(10),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(children: [
          Image.asset(historyIndex.image, width: 56, height: 56,),
          SizedBox(width: 20,),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
            Text(historyIndex.nameAction,
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.w700,
              fontSize: 20,
            ),),
            Text('From ' + historyIndex.paymentName,
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.w400,
              fontSize: 12,
            ),),
            Text(DateFormat('yyyy-MM-dd – kk:mm').format(DateTime.parse(historyIndex.id))),
          ],),

        ],),
        Text(
          '\$${historyIndex.cost}', style: TextStyle(
          color: Colors.red,
          fontWeight: FontWeight.w700,
          fontSize: 20,
        ),),
    ],),
  );
}