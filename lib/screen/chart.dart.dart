import 'package:financeappui/models/history_chart.dart';
import 'package:financeappui/providers/user_auth.dart';
import 'package:financeappui/widgets/indicator.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:provider/provider.dart';

class HistoryChart extends StatefulWidget {
  HistoryChart({Key key}) : super(key: key);

  @override
  _HistoryChartState createState() => _HistoryChartState();
}

class _HistoryChartState extends State<HistoryChart> {
  List<charts.Series<ChartItem, String>> _pieData;

  generateData(AuthUsers authUser){

  List<ChartItem> piedata = authUser.selectedUser.historyChart.chartItems;
  _pieData = [];
  _pieData.add(
    charts.Series(
      domainFn: (ChartItem data, _) => data.name +" ${data.percent.toStringAsFixed(0)}%",
      measureFn: (ChartItem data, _) => data.percent,
      colorFn: (ChartItem data, _) => charts.ColorUtil.fromDartColor(data.color),
      id: 'Money Spend',
      data: piedata,
      labelAccessorFn: (ChartItem data, _) => '\$${data.value.toStringAsFixed(0)}',
    ),
  );
  return _pieData;
  }

  List<Widget> noteWidget(AuthUsers authUser){
    List<ChartItem> chartItems = authUser.selectedUser.historyChart.chartItems;
    List<Widget> indicator = [];
    for(ChartItem chartItem in chartItems){
        Widget indicatorRender = Indicator(
          color: chartItem.color,
          text: chartItem.name,
          isSquare: true,
        );
        indicator.add(indicatorRender);
    }
    return indicator;
  }

@override
Widget build(BuildContext context) {
  Size size = MediaQuery.of(context).size;
  return SafeArea(
    child: Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Text('Personal Spending', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),),),
      body:Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Container(
              margin: EdgeInsets.only(left: 20, right: 20),
              child: Consumer<AuthUsers>(
                builder: (context, authUser, child) {
                  return charts.PieChart(
                    generateData(authUser),
                    animate: true,
                    animationDuration: Duration(seconds: 1),
                    defaultRenderer: charts.ArcRendererConfig(
                      arcWidth: 100,
                      arcRendererDecorators: [
                      charts.ArcLabelDecorator(
                      labelPosition: charts.ArcLabelPosition.inside,
                      insideLabelStyleSpec: charts.TextStyleSpec(fontSize: 20, color: charts.Color.white))
                    ]
                  ),
                );
              }
            )
            ),),
          Container(
            margin: EdgeInsets.only(left: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
              Consumer<AuthUsers>(
              builder: (context, authUser, child) {
                return Container(
                  height: size.height * 0.15,
                  child: Wrap(
                    spacing: 5,
                    runSpacing: 10,
                    direction: Axis. vertical,
                    children: noteWidget(authUser)
                  ),
                );
              }
            ),
            Consumer<AuthUsers>(
              builder: (context, authUser, child) {
                String totalSpend = authUser?.selectedUser?.historyChart?.totalSpendMoney?.toStringAsFixed(2) ?? '0';
                String totalBalance = authUser?.selectedUser?.totalBalance?.toStringAsFixed(2) ?? '0';
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                  Container(child: Text('Total Spend: \$ $totalSpend', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),),
                  Container(child: Text('Total Balance: \$ $totalBalance', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),),
                ],);
            }),
            SizedBox(height: 50,)
          ]),),
        ],),
      ),
    );
  }
}