import 'package:financeappui/providers/user_auth.dart';
import 'package:financeappui/screen/chart.dart.dart';
import 'package:financeappui/screen/history_screem.dart';
import 'package:financeappui/screen/add_friend_screen.dart';
import 'package:financeappui/screen/home_page.dart';
import 'package:financeappui/screen/login_screen.dart';
import 'package:financeappui/screen/setting_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (ctx) => AuthUsers(),),
      ],
      child: MyApp()));
}
class MyApp extends StatefulWidget {
  MyApp({Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      theme: ThemeData(
      primaryColor: Colors.blue,
      fontFamily: 'Avenir'),
      debugShowCheckedModeBanner: false,
      title: 'Finance APP UI',
      home: HomePage(),
      routes: {
        '/history_screen':(context) => HistoryScreen(),
        '/history_chart':(context)=> HistoryChart(),
        '/setting_screen': (context) => SettingScreen(),
        '/login_screen':(context) => LoginScreen(),
        '/add_friend':(context)=>AddFriendScreen(),
      },
    );
  }
}
